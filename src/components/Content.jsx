import Hero from "./content-components/Hero"
import FeatureCard from "./content-components/FeatureCard"
import Demo from "./content-components/Demo"
import Bio from "./content-components/Bio"
import "../styles/content.scss"

const Content = () => {
  return (
    <main>
      <Hero />

      <div className="card-container">
        <FeatureCard
          iconName="keyboard"
          featureName="Easy to implement"
          featureDescription="Easy to implement in your company with powerful platform"
        />
        <FeatureCard
          iconName="paper-plane"
          featureName="Optimize systems"
          featureDescription="More than 1000 company using this products"
        />
      </div>

      <Demo />
      <Bio />
    </main>
  )
}

export default Content
