import brandLogo from "../assets/brand.png"
import "../styles/header.scss"

const Header = () => {
  return (
    <header>
      <a href="/" className="nav-logo">
        <img src={brandLogo} alt="Brand Logo" />
      </a>

      <input type="checkbox" id="check" />
      <label htmlFor="check" className="icons">
        <box-icon name="menu" size="2.6rem" color="#dfdfd6" id="menu-icon" />
        <box-icon name="x" size="2.6rem" color="#dfdfd6" id="close-icon" />
      </label>

      <nav>
        <a href="#" className="active">
          Home
        </a>
        <a
          href="https://wa.me/6285156387262"
          target="_blank"
          rel="noopener noreferrer"
        >
          Contact
        </a>
        <a
          href="https://syamilh.github.io/syamil-cv/"
          target="_blank"
          rel="noopener noreferrer"
        >
          About Me
        </a>
        <a
          href="https://gitlab.com/college-courseworks/semester-v/web-app-development/midterm-exam"
          target="_blank"
          rel="noopener noreferrer"
        >
          Docs
        </a>
      </nav>
    </header>
  )
}

export default Header
