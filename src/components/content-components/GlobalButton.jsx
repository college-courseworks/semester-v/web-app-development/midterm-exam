import PropTypes from "prop-types"
import "../../styles/content-styles/button.scss"

const GlobalButton = (props) => {
  const { buttonText, buttonLink } = props

  return (
    <div className="button-container">
      <a href={buttonLink} target="_blank" rel="noopener noreferrer">
        <button>{buttonText}</button>
      </a>
    </div>
  )
}

GlobalButton.propTypes = {
  buttonText: PropTypes.string.isRequired,
  buttonLink: PropTypes.string.isRequired,
}

export default GlobalButton
