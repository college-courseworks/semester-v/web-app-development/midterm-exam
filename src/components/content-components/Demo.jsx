import GlobalButton from "./GlobalButton"
import heroImage from "../../assets/hero-image.png"
import "../../styles/content-styles/demo.scss"

const Demo = () => {
  return (
    <div className="demo-container">
      <div className="demo-image">
        <img src={heroImage} alt="Demo Image" />
      </div>
      <div className="demo-heading">
        <h2>Now, Set up and grow your company</h2>
        <p>
          We offer many products that optimize your company, setup now and grow
          up your business
        </p>
        <GlobalButton buttonText="Demo" buttonLink="https://github.com/syamilh/" />
      </div>
    </div>
  )
}

export default Demo
