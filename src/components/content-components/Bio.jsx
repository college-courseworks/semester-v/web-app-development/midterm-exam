import GlobalButton from "./GlobalButton"
import "../../styles/content-styles/bio.scss"

const Bio = () => {
  return (
    <>
      <div className="bio-container">
        <div className="bio-name">
          <label htmlFor="name">Name</label>
          <input type="text" id="name" name="name" />
        </div>
        <div className="bio-email">
          <label htmlFor="name">Email</label>
          <input type="text" id="name" name="name" />
        </div>
        <div className="bio-role">
          <label htmlFor="role">Role</label>
          <select id="role" name="role">
            <option value="designer">Designer</option>
            <option value="developer">Developer</option>
            <option value="analyst">Analyst</option>
          </select>
        </div>
      </div>
      <div className="info-container">
        <div className="info-message">
          <label htmlFor="message">Message</label>
          <textarea name="message" id="message" cols="30" rows="20"></textarea>
        </div>
        <div className="info-check">
          <p>I choose to send information about:</p>
          <div className="check-item">
            <input type="checkbox" id="marketing" />
            <label className="checkbox-label" htmlFor="marketing">
              Marketing
            </label>
          </div>
          <div className="check-item">
            <input type="checkbox" id="news" />
            <label className="checkbox-label" htmlFor="news">
              News & Updates
            </label>
          </div>
          <div className="check-item">
            <input type="checkbox" id="productUpdates" />
            <label className="checkbox-label" htmlFor="productUpdates">
              Product Updates
            </label>
          </div>
          <div className="check-item">
            <input type="checkbox" id="none" />
            <label className="checkbox-label" htmlFor="none">
              None
            </label>
          </div>
        </div>
        <GlobalButton buttonText="Submit" buttonLink="#" />
      </div>
    </>
  )
}

export default Bio
