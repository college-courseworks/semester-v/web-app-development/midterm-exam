import GlobalButton from "./GlobalButton"
import heroImage from "../../assets/hero-image.png"
import "../../styles/content-styles/hero.scss"

const Hero = () => {
  return (
    <div id="hero" className="hero-container">
      <div className="hero-image">
        <img src={heroImage} alt="" />
      </div>
      <div className="hero-heading">
        <h1>Revolutionizing Software Delivery with DevOps</h1>
        <p>
          This technology will bring new things and experiences to your business
          and company
        </p>
        <div className="hero-buttons">
          <GlobalButton
            buttonText="Buy"
            buttonLink="https://www.tokopedia.com/"
          />
          <a
            href="https://www.tokopedia.com/"
            className="see-all"
            target="_blank"
            rel="noopener noreferrer"
          >
            See all products
          </a>
        </div>
      </div>
    </div>
  )
}

export default Hero
