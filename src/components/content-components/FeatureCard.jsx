import PropTypes from "prop-types"
import "../../styles/content-styles/featureCard.scss"

const FeatureCard = (props) => {
  const { iconName, featureName, featureDescription } = props

  return (
    <div className="feature-container">
      <box-icon
        name={iconName}
        type="solid"
        color="#fff"
        size="5rem"
      ></box-icon>
      <h3>{featureName}</h3>
      <p>{featureDescription}</p>
    </div>
  )
}

FeatureCard.propTypes = {
  iconName: PropTypes.string.isRequired,
  featureName: PropTypes.string.isRequired,
  featureDescription: PropTypes.string.isRequired,
}

export default FeatureCard
