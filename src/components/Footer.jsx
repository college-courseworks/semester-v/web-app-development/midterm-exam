import "../styles/footer.scss"

const Footer = () => {
  return (
    <footer>
      <div className="footer-container">
        <div className="footer-social">
          <a
            href="https://www.instagram.com/syamil.hamami/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <box-icon type="logo" name="instagram" color="#fff"></box-icon>
          </a>
          <a
            href="https://www.facebook.com/profile.php?id=100082083362158"
            target="_blank"
            rel="noopener noreferrer"
          >
            <box-icon type="logo" name="facebook" color="#fff"></box-icon>
          </a>
          <a
            href="https://www.linkedin.com/in/muhammad-syamil-hamami-112560288/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <box-icon type="logo" name="linkedin" color="#fff"></box-icon>
          </a>
          <a
            href="https://github.com/syamilh"
            target="_blank"
            rel="noopener noreferrer"
          >
            <box-icon type="logo" name="github" color="#fff"></box-icon>
          </a>
        </div>
        <div className="footer-cols">
          <div className="col">
            <a
              href="https://wa.me/6285156387262"
              target="_blank"
              rel="noopener noreferrer"
            >
              Contact
            </a>
            <a
              href="https://syamilh.github.io/syamil-cv/"
              target="_blank"
              rel="noopener noreferrer"
            >
              About Me
            </a>
            <a
              href="https://en.wikipedia.org/wiki/Terms_of_service"
              target="_blank"
              rel="noopener noreferrer"
            >
              Terms & Conditions
            </a>
          </div>
          <div className="col">
            <a
              href="https://en.wikipedia.org/wiki/Career"
              target="_blank"
              rel="noopener noreferrer"
            >
              Careers
            </a>
            <a
              href="https://en.wikipedia.org/wiki/Country"
              target="_blank"
              rel="noopener noreferrer"
            >
              Change Country
            </a>
            <a
              href="https://en.wikipedia.org/wiki/FAQ"
              target="_blank"
              rel="noopener noreferrer"
            >
              FAQ
            </a>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
