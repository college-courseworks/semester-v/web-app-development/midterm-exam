# Landing Page - Web App Development Midterm Exam

1217050103 - Muhammad Syamil Hamami

Praktikum PAW - Kelas F

Web bisa dibuka [disini](https://college-courseworks.gitlab.io/semester-v/web-app-development/midterm-exam/).

# Table of Contents

- [Stack yang Digunakan](#stack-yang-digunakan)
- [Menyiapkan Project](#menyiapkan-project)
- [Penjelasan Komponen Utama](#penjelasan-komponen-utama)
  - [Header](#header)
  - [Content](#content)
  - [Footer](#footer)
- [Penjelasan Komponen Sub](#penjelasan-komponen-sub)

## Stack yang Digunakan

- [ReactJS - Frontend Javascript LIbrary](https://react.dev)
- [Vite - Frontend Build Tools](https://vitejs.dev)
- [SASS - CSS Preprocessor](https://sass-lang.com/)
- [CSS - Styling](https://www.w3schools.com/css/)
- [Gitlab Pages - Deployment](https://docs.gitlab.com/ee/user/project/pages/)

## Menyiapkan Project

Project **React** dibuat menggunakan **Vite** sebagai frontend tools-nya, lalu untuk styling menggunakan **SASS** (Superset dari **CSS**). Berikut ini struktur dari projeknya:

```shell
midterm-exam /
  - src /
    - assets /
    - components /
      - sub-components /
        - {Smaller Components - Button, Icons, etc}.jsx
      - {Components Here}.jsx
    - styles /
      - {All Style File Here}.scss
    - App.jsx
    - main.jsx
    - main.scss
  - .eslintrc.cjs
  - .prettierrc
  - index.html
  - package-lock.json
  - package.json
```

## Penjelasan Komponen Utama

> ***NOTE**: Website Sudah Responsive*

Ada tiga komponen utama yang dibuat, yaitu komponen `Header`, `Content`, dan `Footer`.

### `Header`

Komponen ini berisi semua bagian navigasi, dari mulai logo sampai ke button navigasi untuk tampilan tablet/handphone.

```jsx
import brandLogo from "../assets/brand.png"
import "../styles/header.scss"

const Header = () => {
  return (
    <header>
      // Header Logo, Navigation Icons, etc
      <nav>// Navigation Link Section</nav>
    </header>
  )
}

export default Header
```

### `Content`

Komponen ini berisi semua bagian main konten, dari mulai bagian **Hero** sampai ke tombol submit paling bawah sebelum **Footer**. Komponen ini juga menggunakan komponen-komponen lain dari `sub-components`.

```jsx
import Hero from "./content-components/Hero"
import FeatureCard from "./content-components/FeatureCard"
import Demo from "./content-components/Demo"
import Bio from "./content-components/Bio"
import "../styles/content.scss"

const Content = () => {
  return (
    <main>
      // Bagian Hero
      <Hero />
      // Bagian Cards
      <FeatureCard />
      // Bagian Demo
      <Demo />
      // Bagian Isi Bio
      <Bio />
    </main>
  )
}

export default Content
```

### `Footer`

Komponen terakhir adalah **Footer** yang berisi icon sosial media dan link-link lainnya.

```jsx
import "../styles/footer.scss"

const Footer = () => {
  return (
    <footer>
      // Footer Container
      <div className="footer-container">
        <div className="footer-social">
          // Berisi link icon sosial media
        </div>
        <div className="footer-cols">
          <div className="col">
            // Bagian link yang berada disebelah kanan/atas
          </div>
          <div className="col">
            // Bagian link yang berada disebelah kiri/bawah
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
```

## Penjelasan Komponen Sub

Komponen ini merupakan bagian kecil, seperti **button, icons, card, etc**. Berikut adalah semua komponennya:

- [Bio.jsx](https://gitlab.com/college-courseworks/semester-v/web-app-development/midterm-exam/-/blob/1a49b4b057e4d41a1f2830e672163903d31c674e/src/components/content-components/Bio.jsx) - Bagian Bio pada `Content`
- [Demo.jsx](https://gitlab.com/college-courseworks/semester-v/web-app-development/midterm-exam/-/blob/1a49b4b057e4d41a1f2830e672163903d31c674e/src/components/content-components/Demo.jsx) - Bagian Demo pada `Content`
- [FeatureCard.jsx](https://gitlab.com/college-courseworks/semester-v/web-app-development/midterm-exam/-/blob/1a49b4b057e4d41a1f2830e672163903d31c674e/src/components/content-components/FeatureCard.jsx) - Bagian Cards pada `Content`
- [GlobalButton.jsx](https://gitlab.com/college-courseworks/semester-v/web-app-development/midterm-exam/-/blob/1a49b4b057e4d41a1f2830e672163903d31c674e/src/components/content-components/GlobalButton.jsx) Button yang digunakan diseluruh page
- [Hero.jsx](https://gitlab.com/college-courseworks/semester-v/web-app-development/midterm-exam/-/blob/1a49b4b057e4d41a1f2830e672163903d31c674e/src/components/content-components/Hero.jsx) - Bagian Hero pada `Content`

> ***NOTE**: Klik nama komponen untuk melihat kode lengkapnya.*

